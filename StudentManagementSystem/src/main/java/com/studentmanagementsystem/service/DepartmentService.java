package com.studentmanagementsystem.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.studentmanagementsystem.model.Department;
import com.studentmanagementsystem.repo.DepartmentRepository;



@Component
public class DepartmentService {
	@Autowired
	private DepartmentRepository departmentRepository;

	public Department save(Department department) {
		return departmentRepository.save(department);
	}

	public Optional<Department> getDepartmentById(int departmentId) {

		return departmentRepository.findById(departmentId);
	}

	public Department updateDepartment(Department staff) {

		return departmentRepository.save(staff);
	}

	public void deleteDepartment(int deptid) {
		departmentRepository.deleteById(deptid);

	}

}
