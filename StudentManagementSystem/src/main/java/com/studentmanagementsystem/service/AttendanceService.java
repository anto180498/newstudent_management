package com.studentmanagementsystem.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.studentmanagementsystem.model.Attendance;
import com.studentmanagementsystem.repo.AttendanceRepository;


@Component
public class AttendanceService {
	@Autowired
	public AttendanceRepository attendanceRepository;

	public Attendance save(Attendance attendance) {

		return attendanceRepository.save(attendance);
	}

	public Optional<Attendance> getAttendanceById(int attendanceId) {

		return attendanceRepository.findById(attendanceId);
	}

	public Attendance updateAttendance(Attendance student) {

		return attendanceRepository.save(student);
	}

	public void deleteAttendance(int attdid) {
		attendanceRepository.deleteById(attdid);

	}

}
