package com.studentmanagementsystem.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.studentmanagementsystem.model.Student;
import com.studentmanagementsystem.repo.StudentRepository;





@Component
public class StudentService {
	@Autowired
	private StudentRepository studentRepository;

	public Student save(Student student) {

		return studentRepository.save(student);
	}

	public Optional<Student> getStudentById(int stdid) {

		return studentRepository.findById(stdid);
	}

	public Student updateStudent(Student student1) {

		return studentRepository.save(student1);
	}

	public void deleteStudent(int stdid) {

		studentRepository.deleteById(stdid);
	}

}
