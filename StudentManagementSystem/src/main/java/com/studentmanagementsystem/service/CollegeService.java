package com.studentmanagementsystem.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.studentmanagementsystem.model.College;
import com.studentmanagementsystem.repo.CollegeRepository;

@Component
public class CollegeService {

	@Autowired
	private CollegeRepository collegeRepository;

	public College save(College college) {

		return collegeRepository.save(college);
	}

	public Optional<College> getCollegeById(int id) {

		return collegeRepository.findById(id);
	}

	public College updateCollege(College management) {

		return collegeRepository.save(management);
	}

	public void deleteCollege(int id) {

		collegeRepository.deleteById(id);
	}

}
