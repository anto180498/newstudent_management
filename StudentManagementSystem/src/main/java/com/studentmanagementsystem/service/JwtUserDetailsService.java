package com.studentmanagementsystem.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.studentmanagementsystem.model.College;
import com.studentmanagementsystem.repo.CollegeRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private CollegeRepository collegeRepository;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		College college = collegeRepository.findByUserName(userName).get(0);
		if (college == null) {
			throw new UsernameNotFoundException("User not found with username: " + userName);
		}
		return new org.springframework.security.core.userdetails.User(college.getUserName(), college.getPassWord(),
				new ArrayList<>());

	}
	
	public College save(College college) {
		college.setUserName(college.getUserName());
		college.setPassWord(bcryptEncoder.encode(college.getPassWord()));
		return collegeRepository.save(college);
	}


}
