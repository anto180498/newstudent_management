package com.studentmanagementsystem.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.studentmanagementsystem.model.College;
import com.studentmanagementsystem.model.Department;
import com.studentmanagementsystem.repo.CollegeRepository;
import com.studentmanagementsystem.service.DepartmentService;

@RestController
public class DepartmentController {

	Logger logger = LoggerFactory.getLogger(DepartmentController.class);

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private CollegeRepository collegeRepository;

	@PostMapping("/department/{collegeId}")
	public Department save(@RequestBody Department department, @PathVariable(value = "collegeId") int collegeId) {

		try {
		 
			College college = collegeRepository.findById(collegeId).get();

			if (college != null) {
				department.setCollege(college);
			}
			departmentService.save(department);
			logger.info("post the department details are stored with respect to collegeid"); 
			
		} catch (RuntimeException e) {
			logger.error("error while updating the department.Message:"  + e.getMessage());

		}

		return department;
	}

	@GetMapping("/department/{departmentId}")
	public Optional<Department> getDepartmentById(@PathVariable("departmentId") int departmentId) {
		try {

			logger.info("To getdepartment details with respect to departmetId");
		} catch (RuntimeException e) {
			logger.error("invalid URL or id" + e.getMessage());
		}
		return departmentService.getDepartmentById(departmentId);

	}

	@PutMapping("/department/{departmentId}")
	public Department updateDepartment(@RequestBody Department department,
			@PathVariable("departmentId") int departmentId) {

		try {
			Department staff = departmentService.getDepartmentById(departmentId).get();
			if (staff != null) {

				staff.setDepatmentName(department.getDepatmentName());
				staff.setDepartmentBlock(department.getDepartmentBlock());
				
				departmentService.updateDepartment(staff);
				logger.info("To update the department values with respet to departmentId");
			}
		} catch (RuntimeException e) {
			logger.error("error while updating the department.Message:" + e.getMessage());

		}

		return department;
	}

	@DeleteMapping("/department/{departmentId}")
	public void deleteDepartment(@PathVariable("departmentId") int departmentId) {
		try {
			logger.info("delete the department values with respect to department id");
			departmentService.deleteDepartment(departmentId);

		} catch (RuntimeException e) {
			logger.info("Error while deleting the college record.  Message: " + e.getMessage());
		}
	}
}
