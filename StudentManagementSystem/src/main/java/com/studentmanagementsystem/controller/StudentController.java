package com.studentmanagementsystem.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.studentmanagementsystem.model.Department;
import com.studentmanagementsystem.model.Student;
import com.studentmanagementsystem.repo.DepartmentRepository;
import com.studentmanagementsystem.service.StudentService;

@RestController
public class StudentController {
	
	Logger logger=LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private StudentService studentService;

	@Autowired
	private DepartmentRepository departmentRepository;

	@PostMapping("/student/{departmentId}")
	public Student save(@RequestBody Student student, @PathVariable(value = "departmentId") int departmentId) {
		try{
			Department department = departmentRepository.findById(departmentId).get();
		
		if (department != null) {
			student.setDepartment(department);
		}
		
		logger.info("to post the student details with respect to departmentId");
		studentService.save(student);
		
		}catch(RuntimeException e) {
			logger.error("invalid URL or request method" + e.getMessage());
		}
		
		return student;
	}

	
	@GetMapping("/student/{studentId}")
	public Optional<Student> getStudentById(@PathVariable("studentId") int studentId) {
		
		logger.info("To getstudent details with respect to studentId");
		return studentService.getStudentById(studentId);
	}
	

	@PutMapping("/student/{studentId}")
	public Student updateStudent(@RequestBody Student student, @PathVariable("studentId") int studentId) {
		try {
		Student student1 = studentService.getStudentById(studentId).get();

		if (student1 != null) {

			student1.setStudentName(student.getStudentName());
			student1.setDateOfBrith(student.getDateOfBrith());
			student1.setSection(student.getSection());
			student1.setDate(student.getDate());
			
			logger.info("To update student details with respect to student ID");
			 studentService.updateStudent(student1);
		}
		}catch(RuntimeException e) {
			logger.error("error while updating the student.Message:" + e.getMessage());
		}
		 return student;
	}

	
	@DeleteMapping("/student/{studentId}")
	public void deleteStudent(@PathVariable("studentId") int studentId) {
		
		try {
		
		logger.info("delete the student details with respect to studentID");
		studentService.deleteStudent(studentId);
		
		}catch (RuntimeException e) {
			logger.info("Error while deleting the student record.  Message: " + e.getMessage());
		}
	}
}
