package com.studentmanagementsystem.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.studentmanagementsystem.model.College;
import com.studentmanagementsystem.service.CollegeService;

@RestController
public class CollegeController {

	@Autowired
	private CollegeService collegeService;

	Logger logger = LoggerFactory.getLogger(CollegeController.class);

	@PostMapping("/college")
	public College save(@RequestBody College college) {

		try {

			logger.info("post the college details are stored");
			collegeService.save(college);

		} catch (RuntimeException e) {

			logger.error("invalid URL or request method" + e.getMessage());

		}
		return college;

	}

	@GetMapping("/college/{collegeId}")
	public Optional<College> getCollegeById(@PathVariable("collegeId") int collegeId) {

		try {
			
			logger.info("getcollege details with respect to id");
		} catch (RuntimeException e) {
			logger.error("invalid id or URL" + e.getMessage());
		}
		return collegeService.getCollegeById(collegeId);

	}

	@PutMapping("/college/{collegeId}")
	public College updateCollege(@RequestBody College college, @PathVariable("collegeId") int collegeId) {
		
		try {
			
			College management = collegeService.getCollegeById(collegeId).get();
			if (management != null) {

				management.setCollegeName(college.getCollegeName());
				management.setAddress(college.getAddress());
				management.setLocation(college.getLocation());
				management.setPinCode(college.getPinCode());
			}
			logger.info("update college details are stored with respect to id");
			collegeService.updateCollege(management);
		} catch (RuntimeException e) {
			logger.error("error while updating the college.Message:" + e.getMessage());
		}
		return college;
	}

	@DeleteMapping("/college/{collegeId}")
	public void deleteCollege(@PathVariable("collegeId") int collegeId) {
		
		try {
			logger.info("DELETE METHOD CALLED");
			collegeService.deleteCollege(collegeId);
		
		} catch (RuntimeException e) {

			logger.error("Error while deleting the college record.  Message: " + e.getMessage());

		}
		}

}
