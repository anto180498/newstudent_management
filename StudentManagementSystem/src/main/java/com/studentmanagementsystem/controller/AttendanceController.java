package com.studentmanagementsystem.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.studentmanagementsystem.model.Attendance;
import com.studentmanagementsystem.model.Student;
import com.studentmanagementsystem.repo.StudentRepository;
import com.studentmanagementsystem.service.AttendanceService;

@RestController
public class AttendanceController {
	
	Logger logger=LoggerFactory.getLogger(AttendanceController.class);
	@Autowired
	private AttendanceService attendanceService;

	@Autowired
	private StudentRepository studentRepository;

	@PostMapping("/attendance/{studentId}")
	public Attendance save(@RequestBody Attendance attendance, @PathVariable(value = "studentId") int studentId) {

		try {
			Student student = studentRepository.findById(studentId).get();

			if (student != null) {
				attendance.setStudent(student);
			}

			logger.info("To post the attendance details with respect to studentId");
			attendanceService.save(attendance);
		} catch (RuntimeException e) {
			logger.error("invalid URL or request method" + e.getMessage());

		}

		return attendance;
	}
	
	@GetMapping("/attendance/{attendanceId}")
	public Optional<Attendance> getAttendanceById(@PathVariable("attendanceId") int attendanceId) {
		try {
			logger.info("To getattendance record with respect to attendanceId");
		 }catch (RuntimeException e) {
			 
			 logger.error("invalid URL or id" + e.getMessage());
		 }
		return attendanceService.getAttendanceById(attendanceId);

	}

	@PutMapping("/attendance/{attendanceId}")
	public Attendance updateAttendance(@RequestBody Attendance attendance, @PathVariable("attendanceId") int attendanceId) {
		
		try{
			Attendance student = attendanceService.getAttendanceById(attendanceId).get();
		   if (student != null) {

			student.setDate(attendance.getDate());
			student.setIsPresent(attendance.getIsPresent());
			
			logger.info("To update the attendance record with respect to attendance id");
			attendanceService.updateAttendance(student);
		}
		}catch(RuntimeException e) {
			logger.error("error while updating the department.Message:" + e.getMessage());
			
		}
		return attendance;
	}

	@DeleteMapping("/attendance/{attendanceId}")
	public void deleteAttendance(@PathVariable("attendanceId") int attendanceId) {
		try {
			
		logger.info("delete attendance record with respect to attendanceOId");
		attendanceService.deleteAttendance(attendanceId);
		
		}catch (RuntimeException e) {
			logger.info("Error while deleting the attendance record.  Message: " + e.getMessage());
		}
	}
}
