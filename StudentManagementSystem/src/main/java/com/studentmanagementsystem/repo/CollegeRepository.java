package com.studentmanagementsystem.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.studentmanagementsystem.model.College;



@Repository
public interface CollegeRepository extends JpaRepository<College, Integer> {

	List<College> findByUserName(String userName);

}