package com.studentmanagementsystem.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.studentmanagementsystem.model.Department;



@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {

}
