package com.studentmanagementsystem.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Department {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int departmentId;
	private String depatmentName;
	private int departmentBlock;

	// unidirectional mapping between college and department
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "college_Id")

	public College college;

	public College getCollege() {
		return college;
	}

	public void setCollege(College college) {
		this.college = college;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepatmentName() {
		return depatmentName;
	}

	public void setDepatmentName(String depatmentName) {
		this.depatmentName = depatmentName;
	}

	public int getDepartmentBlock() {
		return departmentBlock;
	}

	public void setDepartmentBlock(int departmentBlock) {
		this.departmentBlock = departmentBlock;
	}

	@Override
	public String toString() {
		return "Department [departmentId=" + departmentId + ", depatmentName=" + depatmentName + ", departmentBlock="
				+ departmentBlock + ", college=" + college + "]";
	}


}
