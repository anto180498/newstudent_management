package com.footballacademy.service;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.footballacademy.Repository.UserRepository;
import com.footballacademy.model.User;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepo;

	public User save(User user) {

		return userRepo.save(user);
	}

	public Optional<User> getUserByid(int id) {

		return userRepo.findById(id);
	}

	public void deleteUser(int id) {
		userRepo.deleteById(id);

	}

	public User updateuser(User user) {

		return userRepo.save(user);
	}

}







	

	
