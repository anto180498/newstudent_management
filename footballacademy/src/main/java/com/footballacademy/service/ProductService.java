package com.footballacademy.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.footballacademy.Repository.ProductRepository;
import com.footballacademy.model.Product;

@Service
public class ProductService {
	@Autowired
	public ProductRepository productRepo;

	public Product save(Product productList) {		
		return productRepo.save(productList);
	}

	public Optional<Product> getProductList(int proid) {
	
		return productRepo.findById(proid);
	}

	public Product Update(Product product1) {
		return productRepo.save(product1);
	}
	

}
