package com.footballacademy.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.footballacademy.Repository.ProductCatRepository;
import com.footballacademy.model.ProductCategeries;

@Component
public class ProductCatService {
	@Autowired
	public ProductCatRepository  productCatRepo;

	public ProductCategeries save(ProductCategeries categeries) {
		
		return  productCatRepo.save(categeries);
	}

	public Optional<ProductCategeries> getProducat(int categoryid) {
		
		return productCatRepo.findById(categoryid);
	}


}
