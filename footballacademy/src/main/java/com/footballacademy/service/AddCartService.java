package com.footballacademy.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.footballacademy.Repository.AddCartRepository;
import com.footballacademy.model.AddCart;

@Component
public class AddCartService {
	@Autowired
	private AddCartRepository addCartRepo;

	public AddCart save(AddCart addCart) {
	
		return addCartRepo.save(addCart);
	}
}
