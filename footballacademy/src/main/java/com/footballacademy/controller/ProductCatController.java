package com.footballacademy.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.footballacademy.model.ProductCategeries;
import com.footballacademy.service.ProductCatService;

@RestController
public class ProductCatController {
	@Autowired
	public ProductCatService productService;

	@PostMapping("/category")
	public ProductCategeries save(@RequestBody ProductCategeries categeries) {
		return productService.save(categeries);
	}

	@GetMapping("/category/{categoryid}")
	public Optional<ProductCategeries> getProductcat(@PathVariable("categoryid") int categoryid) {
		return productService.getProducat(categoryid);
	}


}
