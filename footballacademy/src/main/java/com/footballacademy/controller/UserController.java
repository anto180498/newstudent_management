package com.footballacademy.controller;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.footballacademy.Repository.UserRepository;
import com.footballacademy.model.User;
import com.footballacademy.service.UserService;


@RestController
public class UserController{
	@Autowired
	private UserService userService;
	@Autowired
	private UserRepository userRepo;


	@PostMapping("/userRegister")
	public User save (@RequestBody User user) {
		return userService.save(user);
	}



	@PostMapping("/login")
	public String login(@RequestBody User register)
	{
		User user1 = userRepo.getByEmailAndPassWord(register.getEmail(),register.getPassword());
		
		if(user1 !=null)
		{
			
			return "login success";
		}else
		{
			return "invalid login";
		}
		
	}

	
	@GetMapping("/user/{id}")
	public Optional<User>  getUserByid(@PathVariable("id") int id)
	{
		return userService.getUserByid(id);

	}
	
	@PutMapping("/user/{id}")
    public User Updateuser(@RequestBody User register,@PathVariable("id") int id){
	 User user =userService.getUserByid(id).get();
       
       if(user !=null) {
    	   
    	   user.setUserName(register.getUserName());
    	   user.setEmail(register.getEmail());
    	   user.setMobNo(register.getMobNo());
    	   user.setPassword(register.getPassword());
       }
        return userService.updateuser(user);
	}
                                                                                                                                                                                                              
   @DeleteMapping("/user/{id}")
	public void deleteUser(@PathVariable("id") int id) {
		userService.deleteUser(id);
	}

}
