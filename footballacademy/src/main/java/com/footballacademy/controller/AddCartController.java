package com.footballacademy.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.footballacademy.Repository.ProductRepository;
import com.footballacademy.Repository.UserRepository;
import com.footballacademy.model.AddCart;
import com.footballacademy.model.Product;
import com.footballacademy.model.User;
import com.footballacademy.service.AddCartService;

@RestController
public class AddCartController {
	@Autowired
	private AddCartService cartService;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	public ProductRepository productRepo;

	@PostMapping("/cart/{id}/{proId}")
	public AddCart save(@RequestBody AddCart addCart, @PathVariable(value = "id") int id,
			@PathVariable(value = "proId") int proid)
	{
		List<Product> c = new ArrayList<>();
		User user = userRepo.findById(id).get();
		Product product = productRepo.findById(proid).get();
		if ((user != null) && (product != null)) 
		{
			c.add(product);
			addCart.setUser(user);
			addCart.setProduct(product);
		}
		
		if(addCart.getStatus() ==1)
		{
		product.setCount(product.getCount()-addCart.getCount());
		productRepo.save(product);
		
		}
		
		return cartService.save(addCart);
	}


}
