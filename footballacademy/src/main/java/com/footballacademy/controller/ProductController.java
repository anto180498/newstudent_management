package com.footballacademy.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.footballacademy.Repository.ProductCatRepository;
import com.footballacademy.model.Product;
import com.footballacademy.model.ProductCategeries;
import com.footballacademy.service.ProductService;

@RestController
public class ProductController {
	@Autowired
	public ProductService productService;

	@Autowired
	public ProductCatRepository  productCatRepo;

	@PostMapping("/product/{categoryId}")

	public Product save(@RequestBody Product productList, @PathVariable(value = "categoryId") int categoryId) {
		ProductCategeries categeries = productCatRepo.findById(categoryId).get();
		if (categeries != null) {
			productList.setProductCategeries(categeries);
		}
		return productService.save(productList);
	}

	@GetMapping("/product/{proId}")
	public Optional<Product> getProductList(@PathVariable("proId") int proid) {
		return productService.getProductList(proid);
	}
	
	@PutMapping("/product/{proId}")
    public Product Update(@RequestBody Product product,@PathVariable("proId") int proId){
    	Product product1 =productService.getProductList(proId).get();
       
       if(product1 !=null) {
    	   
    	   product1.setProductName(product.getProductName());
    	   product1.setPrice(product.getPrice());
    	   product1.setCount(product.getCount());
    	   product1.setSize(product.getSize());
    	  
       }
        return productService.Update(product1);
	}
}
