package com.footballacademy.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.footballacademy.model.User;



@Repository
public interface UserRepository  extends JpaRepository <User, Integer> {
	
@Query("SELECT r FROM User r where r.email =:email And r.password=:password")         
 public	User getByEmailAndPassWord(String email,long password);


}
