package com.footballacademy.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.footballacademy.model.AddCart;

@Repository
public interface AddCartRepository extends JpaRepository <AddCart ,Integer> {

}
