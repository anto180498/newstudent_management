package com.footballacademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootballacademyApplication {

	public static void main(String[] args) {
		SpringApplication.run(FootballacademyApplication.class, args);
	}

}
