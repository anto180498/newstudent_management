package com.footballacademy.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int proId;
	private String productName;
	private int price;
	private int size;
	private int count;


	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "category_id")
	public ProductCategeries productCategeries;

	public ProductCategeries getProductCategeries() {
		return productCategeries;
	}

	public void setProductCategeries(ProductCategeries productCategeries) {
		this.productCategeries = productCategeries;
	
	}
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	 
	private AddCart addCart;
	
	public AddCart getAddCart() {
		return addCart;
	}

	public void setAddCart(AddCart addCart) {
		this.addCart = addCart;
	}

	
	public int getProId() {
		return proId;
	}

	public void setProId(int proId) {
		this.proId = proId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Product [proId=" + proId + ", productName=" + productName + ", price=" + price + ", size=" + size
				+ ", count=" + count + ", productCategeries=" + productCategeries + ", addCart=" + addCart + "]";
	}
	
	}
