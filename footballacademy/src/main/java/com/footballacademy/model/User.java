package com.footballacademy.model;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
public class User {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String userName;
	private long password;
	private long mobNo;
	private String email;
	
	@OneToMany(targetEntity = AddCart.class,cascade = CascadeType.ALL)
//	@JoinColumn(name="user_id",referencedColumnName = "id")
//	@JsonIgnoreProperties("user")
//	public List <AddCart> addCart;
//	
//	public List<AddCart> getAddCart() {
//		return addCart;
//	}
//	public void setAddCart(List<AddCart> addCart) {
//		this.addCart = addCart;
//	}
//	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getPassword() {
		return password;
	}
	public void setPassword(long password) {
		this.password = password;
	}
	public long getMobNo() {
		return mobNo;
	}
	public void setMobNo(long mobNo) {
		this.mobNo = mobNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", password=" + password + ", mobNo=" + mobNo + ", email="
				+ email + "]";
	}
	

	}
	
	
	
	

	

