package com.footballacademy.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class AddCart {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int count;
	private String date;
	private int status;


    @ManyToOne(targetEntity = User.class)
    @JsonIgnoreProperties("AddCart")
    private User user;
 
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JsonIgnoreProperties("Product")
	private  Product product;
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AddCart [id=" + id + ", count=" + count + ", date=" + date + ", status=" + status + ", user=" + user
				+ ", product=" + product + "]";
	}


}
	

